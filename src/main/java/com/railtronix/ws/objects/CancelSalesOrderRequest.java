package com.railtronix.ws.objects;

import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.railtronix.ws.patterns.Request;


@XmlRootElement(name="CancelSalesOrderRequest")
public class CancelSalesOrderRequest extends Request {
	
	@XmlElement(name="PurchaseOrderNumber")
	String PurchaseOrderNumber;
	
	@XmlElement(name="SalesOrderNumber")
	String SalesOrderNumber;
	
	public String getPurchaseOrderNumber() {
		return PurchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		PurchaseOrderNumber = purchaseOrderNumber;
	}
	public String getSalesOrderNumber() {
		return SalesOrderNumber;
	}
	public void setSalesOrderNumber(String salesOrderNumber) {
		SalesOrderNumber = salesOrderNumber;
	}
	
	
}


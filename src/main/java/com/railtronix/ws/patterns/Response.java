package com.railtronix.ws.patterns;

import javax.xml.bind.annotation.XmlElement;

import com.railtronix.ws.objects.ResponseHeader;

public class Response {
	
	@XmlElement(name="Header")
	ResponseHeader header;
	
	public void setHeader(ResponseHeader header) {
		this.header = header;
	}
	
}

package com.railtronix.ws.exceptions;

public class ValidationException extends Exception{

	private static final long serialVersionUID = -1690868599163089277L;

	private final static String CODE_ERROR = "SO-1001";
	private final static String DESCRIPTION = "Validation error. One or more elements in the request does not confirm to the interface schema.";
	
	public ValidationException(){
		super(CODE_ERROR+","+DESCRIPTION);
	}

}

package com.railtronix.ws.patterns;

import javax.xml.bind.annotation.XmlElement;

public class Order {
	
  @XmlElement(name="Header")	
  OrderHeader Header;
  
  @XmlElement(name="Detail") 
  Detail[] Detail;

   public OrderHeader getHeader() {
	return Header;
   }
 	
  public void setHeader(OrderHeader header) {
	Header = header;
  }

  public Detail[] getDetail() {
	return Detail;
  }

  public void setDetail(Detail[] detail) {
	Detail = detail;
  }
}

package com.railtronix.ws.patterns;

import javax.xml.bind.annotation.XmlElement;

public class Header {
	
  @XmlElement(name="RequestID")	
  private String RequestID;
  @XmlElement(name="RequestDate")
  private String RequestDate;

  public String getRequestID (){
   return RequestID;
  }

  public void setRequestID (String RequestID){
    this.RequestID = RequestID;
  }

  public String getRequestDate (){
    return RequestDate;
  }

  public void setRequestDate (String RequestDate){
     this.RequestDate = RequestDate;
  }

}

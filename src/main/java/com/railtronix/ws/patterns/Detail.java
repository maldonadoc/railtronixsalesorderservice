package com.railtronix.ws.patterns;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Detail {
	
	@XmlElement(name="Status")
	private String Status;
	@XmlElement(name="LineNumber")
	private String LineNumber;
	@XmlElement(name="Quantity")
	private String Quantity;
	@XmlElement(name="RequestDate")
	private String RequestDate;
	@XmlElement(name="GradeItem")
	private String GradeItem;
	
	private String UOM;
	private String action;
	private String Created;
	private String lastUpdated;
    
    public String getStatus (){
        return Status;
    }

    public void setStatus (String Status){
        this.Status = Status;
    }

    public String getLineNumber (){
        return LineNumber;
    }

    public void setLineNumber (String LineNumber){
        this.LineNumber = LineNumber;
    }

    public String getQuantity (){
        return Quantity;
    }

    public void setQuantity (String Quantity){
        this.Quantity = Quantity;
    }

    public String getRequestDate (){
        return RequestDate;
    }

    public void setRequestDate (String RequestDate){
        this.RequestDate = RequestDate;
    }

    public String getGradeItem (){
        return GradeItem;
    }

    public void setGradeItem (String GradeItem){
        this.GradeItem = GradeItem;
    }

    @XmlAttribute
    public String getAction (){
        return action;
    }

    public void setAction (String action){
        this.action = action;
    }

    public String getUOM (){
        return UOM;
    }

    public void setUOM (String UOM){
        this.UOM = UOM;
    }
    
    @XmlAttribute
	public String getCreated() {
		return Created;
	}

	public void setCreated(String created) {
		Created = created;
	}
	
	@XmlAttribute
	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
}

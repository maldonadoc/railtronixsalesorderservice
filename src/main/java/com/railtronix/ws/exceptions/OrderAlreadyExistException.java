package com.railtronix.ws.exceptions;

public class OrderAlreadyExistException extends Exception{

	private static final long serialVersionUID = 5626815442547620686L;
	private final static String CODE_ERROR = "SO-1002";
	private final static String DESCRIPTION= "Duplicate order - order already exists in the TLP system";
	
	public OrderAlreadyExistException(){
		super(CODE_ERROR+","+DESCRIPTION);
	}
}

package com.railtronix.ws.objects;

import javax.xml.bind.annotation.XmlElement;

public class ResponseFault {

	@XmlElement(name="FaultCode")
	String faultCode;
	@XmlElement(name="FaultDescription")
	String faultDescription;

	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}

	public void setFaultDescription(String faultDescription) {
		this.faultDescription = faultDescription;
	}

	
}

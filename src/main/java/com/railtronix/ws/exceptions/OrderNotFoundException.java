package com.railtronix.ws.exceptions;

public class OrderNotFoundException extends Exception {

	private static final long serialVersionUID = 621167876136720196L;
	private final static String CODE_ERROR = "SO-1003";
	private final static String DESCRIPTION = "Sales order number does not exist in the TLP.";
	
	public OrderNotFoundException(){
		super(CODE_ERROR+","+DESCRIPTION);
	}
}

package com.railtronix.ws.utils;

import java.io.InputStream;
import java.util.Properties;


public class Loader 
{
	private Properties 	props = new Properties();
	private Properties 	sysProps = new Properties();
	private String 		filename = new String(); 

	public Loader(String filename)throws Exception
	{
		this.filename = filename;
		this.loadProperties();
	}
	
	public Loader(Properties props)throws Exception
	{
		this.props = props;
	}
	
	private String getValue(String key)throws Exception
	{
		String value = null;
		
		value = sysProps.getProperty(key);
		
		if(value != null)
			return value.trim();
		
		value = props.getProperty(key);
		
		if(value == null)
			throw new Exception("The parameter: " + key + " was not found in the " + this.filename + " file");
		
		return value.trim();
	}
	
	public String getString(String key) throws Exception
	{
		return this.getValue(key);
	}
	
	public int getInt(String key) throws Exception
	{
		return Integer.parseInt(this.getValue(key));
	}
	
	public boolean getBoolean(String key)throws Exception
	{
		String value = this.getValue(key);
		
		if(value.toLowerCase().equals("true"))
			return true;
		else if(value.toLowerCase().equals("false"))
			return false;
		else
			throw new Exception("The property: " + key + " was requested as a true/false value but your value is: " + value);
		
	}
	
	
	/**
	 * This method is the only one that is not static. It is important to leave
	 * it like that, since a static method would not get a refernce to the
	 * ClassLoader since no loader would load it as static
	 * 
	 * @param propertiesFile
	 *            the name of the file tg2Plugin.properties or any other
	 *            relative to the place where the jar is stored
	 * @return an instanciated object properties with all the properties in the
	 *         requested file.
	 * @throws Exception
	 *             if the Class loader is not found or if the properties File is
	 *             not found
	 */
	
	private void loadProperties()throws Exception
	{
		
		this.sysProps = System.getProperties();

		
			ClassLoader loader = this.getClass().getClassLoader();
			
			if(loader == null)
				throw new Exception("Class Loader Was not found") ;
			
			InputStream istream = loader.getResourceAsStream (this.filename);

			if(istream == null)
				throw new Exception("File: " + this.filename + " was not found") ;
			
			try 
			{
				this.props.load(istream);
				istream.close();
			} 
			catch (Exception e)
			{
				throw new Exception(e);
			}
			
	}
	
	public String toString()
	{
		return this.props.toString();
	}
	
}

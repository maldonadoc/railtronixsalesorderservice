@XmlSchema(
    namespace="http://com.ussilica.soa/edi/salesorder/v1",
    xmlns={
        @XmlNs(prefix="so", namespaceURI="http://com.ussilica.soa/edi/salesorder/v1")
    }
)
package com.railtronix.ws.objects;
import javax.xml.bind.annotation.*;
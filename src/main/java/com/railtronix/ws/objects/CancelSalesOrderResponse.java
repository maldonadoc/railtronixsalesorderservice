package com.railtronix.ws.objects;

import javax.xml.bind.annotation.XmlRootElement;

import com.railtronix.ws.patterns.Response;

@XmlRootElement(name="CancelSalesOrderResponse")
public class CancelSalesOrderResponse extends Response{}

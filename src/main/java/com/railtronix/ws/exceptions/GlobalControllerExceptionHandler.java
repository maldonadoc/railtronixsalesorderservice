package com.railtronix.ws.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.railtronix.ws.objects.CreateSalesOrderResponse;
import com.railtronix.ws.objects.ResponseFault;
import com.railtronix.ws.objects.ResponseHeader;


@ControllerAdvice
public class GlobalControllerExceptionHandler {
	
	private final static String VALIDATION_ERROR = "SO-1001";
	private final static String VALIDATION_MESSAGE = "Validation error. One or more elements in the request does not confirm to the interface schema.";
	
	@ExceptionHandler(BadRequestException.class)	
	public CreateSalesOrderResponse validationError(BadRequestException e){
		CreateSalesOrderResponse resp = new CreateSalesOrderResponse();
		ResponseHeader header = new ResponseHeader();
		header.setRequestID(e.getRequestId());
		header.setResponseDate(e.getRequestDate());
		ResponseFault fault = new ResponseFault();
		fault.setFaultCode(VALIDATION_ERROR);
		fault.setFaultDescription(VALIDATION_MESSAGE);
		header.setFault(fault);
		resp.setHeader(header);
		return resp;
	} 

}

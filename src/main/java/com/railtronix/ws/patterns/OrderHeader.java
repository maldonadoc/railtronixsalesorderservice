package com.railtronix.ws.patterns;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class OrderHeader {
	
  @XmlElement(name="PurchaseOrderNumber")
  private String PurchaseOrderNumber;
  @XmlElement(name="BranchPlant")
  private String BranchPlant;
  @XmlElement(name="Quantity")
  private String Quantity;
  @XmlElement(name="SalesOrderNumber")
  private String SalesOrderNumber;
  @XmlElement(name="ShipTo")
  private Shipping ShipTo;
  @XmlElement(name="DeliveryInstructions")
  private String DeliveryInstructions;
  @XmlElement(name="WellName")
  private String WellName;
  
  private String UOM;
  private String Action;
  private String Created;
  private String lastUpdated;
  
    public String getWellName() {
	  return WellName;
    }
    public void setWellName(String wellName) {
	  WellName = wellName;
    }
	public String getDeliveryInstructions() {
		return DeliveryInstructions;
	}
    public void setDeliveryInstructions(String deliveryInstructions) {
		DeliveryInstructions = deliveryInstructions;
	}
    @XmlAttribute
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getPurchaseOrderNumber() {
		return PurchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		PurchaseOrderNumber = purchaseOrderNumber;
	}
	public String getBranchPlant() {
		return BranchPlant;
	}
	public void setBranchPlant(String branchPlant) {
		BranchPlant = branchPlant;
	}
	public String getQuantity() {
		return Quantity;
	}
	public void setQuantity(String quantity) {
		Quantity = quantity;
	}
	public String getSalesOrderNumber() {
		return SalesOrderNumber;
	}
	public void setSalesOrderNumber(String salesOrderNumber) {
		SalesOrderNumber = salesOrderNumber;
	}
	public String getUOM() {
		return UOM;
	}
	public void setUOM(String uOM) {
		UOM = uOM;
	}
	public Shipping getShipTo() {
		return ShipTo;
	}
	public void setShipTo(Shipping shipTo) {
		ShipTo = shipTo;
	}
	
	@XmlAttribute
	public String getAction() {
		return Action;
	}
	public void setAction(String action) {
		Action = action;
	}
	@XmlAttribute
	public String getCreated() {
		return Created;
	}
	public void setCreated(String created) {
		Created = created;
	}
}

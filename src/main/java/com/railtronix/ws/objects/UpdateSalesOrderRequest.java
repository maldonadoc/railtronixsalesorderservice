package com.railtronix.ws.objects;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "UpdateSalesOrderRequest")
public class UpdateSalesOrderRequest extends CreateSalesOrderRequest{
	
}

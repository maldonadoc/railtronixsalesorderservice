package com.railtronix.ws.objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.railtronix.ws.patterns.Response;

@XmlRootElement(name="CreateSalesOrderResponse")
public class CreateSalesOrderResponse extends Response {

	@XmlElement(name="OrderNumber")
	String orderNumber;
	
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
}

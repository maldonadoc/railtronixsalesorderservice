package com.railtronix.ws.objects;

import javax.xml.bind.annotation.XmlElement;


public class ResponseHeader {
	
	@XmlElement(name="RequestID")
	String requestID;
	@XmlElement(name="ResponseDate")
	String responseDate;
	@XmlElement(name="Status")
	String status;
	@XmlElement(name="Fault")
	ResponseFault fault;
	
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	public void setResponseDate(String responseDate) {
		this.responseDate = responseDate;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setFault(ResponseFault fault) {
		this.fault = fault;
	}	
	
}

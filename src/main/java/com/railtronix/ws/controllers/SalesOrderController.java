package com.railtronix.ws.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.railtronix.ws.exceptions.BadRequestException;
import com.railtronix.ws.objects.CreateSalesOrderRequest;
import com.railtronix.ws.objects.CreateSalesOrderResponse;

@RestController
@RequestMapping(
		value = "/salesorder",
		produces = MediaType.APPLICATION_XML_VALUE,
		consumes = MediaType.APPLICATION_XML_VALUE)
public class SalesOrderController {
	private static final Logger log = LoggerFactory.getLogger(SalesOrderController.class);
	
	@RequestMapping(method= RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED) 
	public CreateSalesOrderResponse createSalesOrder(@RequestBody CreateSalesOrderRequest request){
	  if(request == null){ throw new BadRequestException(null,null);}
	  log.info("not null request");
	  return new CreateSalesOrderResponse();
	} 
}

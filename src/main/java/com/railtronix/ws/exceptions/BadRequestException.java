package com.railtronix.ws.exceptions;

import org.apache.commons.lang.StringUtils;

public class BadRequestException extends RuntimeException {
	
	private static final long serialVersionUID = 7724021283612356902L;
	private String requestId;
	private String requestDate;
	
	public BadRequestException(String requestId, String requestDate) {
		super("Create Sales Order Request is wrong or null ");
		this.requestId = (StringUtils.trimToNull(requestId) == null) ?  "" :  requestId;
		this.requestDate = (StringUtils.trimToNull(requestDate) == null) ?  "" :  requestDate;
	}

	public String getRequestId() {
		return requestId;
	}


	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}


	public String getRequestDate() {
		return requestDate;
	}


	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	
}

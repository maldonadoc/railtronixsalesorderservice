package com.railtronix.ws.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

//import com.railtornix.bridge.ws.connection.SessionHandler;
//import com.railtornix.bridge.ws.to.StubRQ;
import com.railtronix.ws.exceptions.ValidationException;
import com.railtronix.ws.objects.CancelSalesOrderRequest;
import com.railtronix.ws.objects.ResponseFault;
import com.railtronix.ws.objects.ResponseHeader;
import com.railtronix.ws.patterns.OrderHeader;
import com.railtronix.ws.patterns.Request;
import com.railtronix.ws.utils.Loader;


public final class SalesOrderUtils {
	
	private final static Logger logger = Logger.getLogger(SalesOrderUtils.class);
	private final static String UOMValid = "TON TN LB";
	
	private SalesOrderUtils(){}
	
	public static void isOrderHeaderValid(OrderHeader orderHeader) throws ValidationException{
		if(!(validValue(orderHeader.getSalesOrderNumber(),"SalesOrderNumber" ) &&
			 validValue(orderHeader.getPurchaseOrderNumber(), "PurchaseOrderNumber") && 
			 validValue(orderHeader.getQuantity(), "Quantity") &&
			 validValue(orderHeader.getAction(), "Action") &&
			 validUOM(orderHeader.getUOM()) &&
			 orderHeader.getShipTo() != null)){
			throw new ValidationException();
		}
	}
	
	public static void isCancelRequestValid(CancelSalesOrderRequest request) throws ValidationException{
		if(!(validValue(request.getPurchaseOrderNumber(), "PurchaseOrderNumber") &&
			  validValue(request.getSalesOrderNumber(),"SalesOrderNumber" ))){
			throw new ValidationException();
		}
	}
	
	private static Boolean validUOM(String UOM){
		String UOMValue = StringUtils.trimToNull(UOM);
		return ( validValue(UOMValue, "UOM") && UOMValid.contains(UOMValue) );
	}
	
	private static Boolean validValue(String value, String nameValue){
		String valueClean = StringUtils.trimToNull(value);
		logger.warn("Validating "+nameValue+": " +  value);
		return (valueClean != null);
	}
	
	public static ResponseHeader initializeResponseHeader(Request request) {
		ResponseHeader header = new ResponseHeader();
		header.setRequestID(request.getHeader().getRequestID());
		header.setResponseDate(request.getHeader().getRequestDate());
		header.setStatus("Error");
		return header;
    }

	public static void fillValidErrorResponse(ResponseHeader header, String code, String description) {
		ResponseFault fault = new ResponseFault();
		fault.setFaultCode(code);
		fault.setFaultDescription(description);		
		header.setFault(fault);
	}
	
	public static Loader getLoader(String fileName) throws Exception{
		try{
			Loader loader = new Loader(fileName);
			return loader;
		}catch(Exception e){
			logger.fatal("Not found properties(" + fileName + ").",e);
			throw new Exception("Not found properties");
		}
	}
	
	/*public static SessionHandler createSession(Loader ws) throws ValidationException{
		try{
			SessionHandler session = new SessionHandler();
			StubRQ rq = new StubRQ(ws.getString("host.enterprise"));
			session.setEndPoint(ws.getString("host.open.session"));
			session.setRq(rq);
			return session;
		}catch(Exception e){
			logger.error("Failed creating SessionHandler.",e);
			throw new ValidationException();
		}
	}*/	

}

package com.railtronix.ws.patterns;

import javax.xml.bind.annotation.XmlElement;

public class Request {
	
	@XmlElement(name="Header")
	Header Header;

	public Header getHeader() {
		return Header;
	}

	public void setHeader(Header header) {
		Header = header;
	}
	
}

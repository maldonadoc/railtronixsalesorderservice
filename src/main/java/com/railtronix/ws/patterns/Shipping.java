package com.railtronix.ws.patterns;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Shipping {
  @XmlElement(name="MailingName")	
  private String MailingName;	
  @XmlElement(name="Id")
  private String Id;
  @XmlElement(name="Line1")
  private String Line1;
  @XmlElement(name="Line2")
  private String Line2;
  @XmlElement(name="Line3")
  private String Line3;
  @XmlElement(name="PostalCode")
  private String PostalCode;
  @XmlElement(name="State")
  private String State;
  @XmlElement(name="CountryCode")
  private String CountryCode;
  @XmlElement(name="City")
  private String City;
  
    @XmlAttribute
    public String getId() {
		return Id;
	}
	public void setId(String id) {
		this.Id = id;
	}
	public String getLine1() {
		return Line1;
	}
	public void setLine1(String line1) {
		Line1 = line1;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getLine2() {
		return Line2;
	}
	
	public void setLine2(String line2) {
		Line2 = line2;
	}
	
	public String getLine3() {
		return Line3;
	}
	
	public void setLine3(String line3) {
		Line3 = line3;
	}
	public String getMailingName() {
		return MailingName;
	}
	public void setMailingName(String mailingName) {
		MailingName = mailingName;
	}
	
	  
}

package com.railtronix.ws.objects;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.railtronix.ws.patterns.Order;
import com.railtronix.ws.patterns.Request;

@XmlRootElement(name="CreateSalesOrderRequest")
public class CreateSalesOrderRequest extends Request{

	@XmlElement(name="Order")
	Order Order;

	public Order getOrder() {
		return Order;
	}
	
	public void setOrder(Order order) {
		Order = order;
	}
	
}
